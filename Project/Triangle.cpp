#include <iostream>
#include <cmath>
#include "Triangle.h"

using namespace std;

Triangle::Triangle() : Shape()
{
	a = 0.0;
	b = 0.0;
	c = 0.0;
}

bool Triangle::SetData()
{
	cout << "============ TRIANGLE ============\n\n";

	cout << "Enter side a:\n> ";
	cin >> a;

	cout << "Enter side b:\n> ";
	cin >> b;

	cout << "Enter side c:\n> ";
	cin >> c;

	return ValidateData(a, b, c);
}

void Triangle::Calculate()
{
	perimeter = a + b + c;
	
	double p = perimeter / 2;

	area = sqrt(p*(p-a)*(p-b)*(p-c));
}


void Triangle::PrintResult()
{
	cout << "============ TRIANGLE ============\n\n";
	cout << "> side a = " << a;
	cout << "\n> side b = " << b;
	cout << "\n> side c = " << c;
	cout << "\n\n> perimeter = " << perimeter;
	cout << "\n> area = " << area;
	cout << "\n\n";
	cout << "**********************************\n";
	cout << "*     Press Enter to continue    *\n";
	cout << "**********************************\n";
	
}

char * Triangle::GetName()
{
	return "Triangle";
}