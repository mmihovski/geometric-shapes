#pragma once 

#include "Shape.h"

class Triangle : public Shape
{
	private:
		double a;
		double b;
		double c;

	public:
		Triangle::Triangle();

		bool SetData();
		void Calculate();
		void PrintResult();
		char* GetName();
};