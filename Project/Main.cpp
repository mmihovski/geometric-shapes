#include <iostream>
#include <fstream>
#include <iomanip>
#include "Shape.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Square.h"
#include "Triangle.h"

#define N 100

using namespace std;

void ExportToFile(int n, Shape *p[]);

int main()
{
	int i = 0;
	int choise;

	Shape * f[N];
	
	while (1)
	{
		system("cls");

		cout << "******************************************************\n";
		cout << "*                         MENU                       *\n";
		cout << "*                                                    *\n";
		cout << "*     Enter the number to choose operation           *\n";
		cout << "*                                                    *\n";
		cout << "*         1) Square                                  *\n";
		cout << "*         2) Rectangle                               *\n";
		cout << "*         3) Triangle                                *\n";
		cout << "*         4) Circle                                  *\n";
		cout << "*                                                    *\n";
		cout << "*         9) Export Data                             *\n";
		cout << "*                                                    *\n";
		cout << "*         0) Quit                                    *\n";
		cout << "*                                                    *\n";
		cout << "******************************************************\n";

		cout << "\nYour choise is:\n> ";
		cin >> choise;

		system("cls");

		
		switch (choise)
		{
			case 1:
				f[i] = new Square();
			break;

			case 2:
				f[i] = new Rectangle();
			break;

			case 3:
				f[i] = new Triangle();
			break;

			case 4:
				f[i] = new Circle();
			break;

			case 9:
			{
				ExportToFile(i, f);

				cout << "> The data has been wrote in file\n";
				cout << "\n\n";
				cout << "**********************************\n";
				cout << "*     Press Enter to continue    *\n";
				cout << "**********************************\n";

				fflush(stdin);
				getchar();
				continue;

			}
			break;

			case 0:
			{
				cout << "======= End of the program =======\n\n";

				for (int k = 0; k < i; k++)
					delete f[k];

				system("pause");
				return 0;
			}
			break;

			default:
			{
				cout << "> You entered wrong number\n";
				cout << "\n\n";
				cout << "**********************************\n";
				cout << "*     Press Enter to continue    *\n";
				cout << "**********************************\n";
				fflush(stdin);
				getchar();
				continue;
			}
			break;

		}

		if (f[i]->SetData() == false)
		{
			system("cls");
			cout << "> The shape is undefined\n";
			cout << "> Please insert valid values\n";
			cout << "\n\n";
			cout << "**********************************\n";
			cout << "*     Press Enter to continue    *\n";
			cout << "**********************************\n";
			fflush(stdin);
			getchar();
			continue;
		}

		system("cls");

		f[i]->Calculate();
		f[i]->PrintResult();

		i++;

		fflush(stdin);
		getchar();
	}
	
	system("pause");
	return 0;
}

void ExportToFile(int n, Shape *p[])
{
	ofstream Wfile("exported_data.txt");

	Wfile << "+-----------------------------------------------------+\n";
	Wfile << "|" << setw(5) << "#" << setw(15) << "FIGURE" << setw(15) << "PERIMETER" << setw(15) << "AREA" << setw(5) << "|\n";
	Wfile << "+-----------------------------------------------------+\n";

	for (int j = 0; j < n; j++)
	{
		Wfile << "|";
		Wfile << setw(5) << j+1 << setw(15);
		Wfile << setw(15) << p[j]->GetName() << setw(15);
		Wfile << setw(15) << p[j]->GetPerimeter() << setw(15);
		Wfile << setw(15) << p[j]->GetArea() << setw(15);
		Wfile << setw(5) << "|\n";
	}

	Shape total;

	for (int k = 0; k < n; k++)
	{
		total = *p[k] + total;
		//total = p[k]->operator+(total);
	}

	Wfile << "+-----------------------------------------------------+\n";
	Wfile << "|" << setw(20) << "TOTAL" << setw(15) << total.GetPerimeter() << setw(15) << total.GetArea() << setw(5) << "|\n";
	Wfile << "+-----------------------------------------------------+\n";

	Wfile.close();

}