#include <iostream>
#include "Rectangle.h"

using namespace std;

Rectangle::Rectangle() :Shape()
{
	lenght = 0.0;
	width = 0.0;
}

bool Rectangle::SetData()
{
	cout << "=========== RECTANGLE ============\n\n";

	cout << "Enter lenght:\n> ";
	cin >> lenght;

	cout << "Enter width:\n> ";
	cin >> width;

	return ValidateData(lenght, width);
}

void Rectangle::Calculate()
{
	perimeter = 2 * lenght + 2 * width;
	area = lenght * width;
}

void Rectangle::PrintResult()
{
	cout << "=========== RECTANGLE ============\n\n";
	cout << "> lenght = " << lenght;
	cout << "\n> width = " << width;
	cout << "\n\n> perimeter = " << perimeter;
	cout << "\n> area = " << area;
	cout << "\n\n";
	cout << "**********************************\n";
	cout << "*     Press Enter to continue    *\n";
	cout << "**********************************\n";
}

char * Rectangle::GetName()
{
	return "Rectangle";
}