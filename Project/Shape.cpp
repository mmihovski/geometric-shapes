#include "Shape.h"
#include <cmath>

using namespace std;
 
Shape::Shape()
{
	area = 0.0;
	perimeter = 0.0;
}

bool Shape::SetData()
{
	Shape();
	return true;
}

char* Shape::GetName()
{
	return "Shape";
}

double Shape::GetArea()
{
	return area;
}

double Shape::GetPerimeter()
{
	return perimeter;
}

bool Shape::ValidateData(double a)
{
	if (a <= 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Shape::ValidateData(double a, double b)
{
	if (a <= 0 || b <= 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Shape::ValidateData(double a, double b, double c)
{
	if (a <= 0 || b <= 0 || c <= 0)
	{
		return false;
	}
	else
	{
		double p = (a + b + c) / 2;
		double area = sqrt(p*(p - a)*(p - b)*(p - c));

		if (area == 0 || isnan(area))
			return false;
	}
	return true;
}

Shape Shape::operator+(Shape &obj)
{
	Shape temp;

	temp.perimeter = perimeter + obj.perimeter;
	temp.area = area + obj.area;

	return temp;
}




