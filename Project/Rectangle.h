#pragma once 

#include "Shape.h"

class Rectangle : public Shape
{
	private:
		double lenght;
		double width;

	public:
		Rectangle::Rectangle();
	
		bool SetData();
		void Calculate();
		void PrintResult();
		char* GetName();
};