#include <iostream>
#include "Circle.h"

#define PI 3.14159

using namespace std;

Circle::Circle() : Shape()
{
	radius = 0.0;
}

bool Circle::SetData()
{
	cout << "============= CIRCLE =============\n\n";
	cout << "Enter radius:\n> ";

	cin >> radius;

	return ValidateData(radius);
}

void Circle::Calculate()
{
	perimeter = 2 * radius * PI;
	area = PI * radius * radius;
}

void Circle::PrintResult()
{
	cout << "============= CIRCLE =============\n\n";
	cout << "> radius = " << radius;
	cout << "\n\n> perimeter = " << perimeter;
	cout << "\n> area = " << area;
	cout << "\n\n";
	cout << "**********************************\n";
	cout << "*     Press Enter to continue    *\n";
	cout << "**********************************\n";
}

char *Circle::GetName()
{
	return "Circle";
}