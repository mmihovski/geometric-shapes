#pragma once 

class Shape
{
	protected:
		double area;
		double perimeter;

		bool ValidateData(double a);
		bool ValidateData(double a, double b);
		bool ValidateData(double a, double b, double c);

	public:
		Shape::Shape();

		virtual bool SetData();
		virtual void Calculate() {};
		virtual void PrintResult() {};
		virtual char *GetName();
		double GetArea();
		double GetPerimeter();
		Shape operator+ (Shape &obj2);
};
