#include <iostream>
#include "Square.h"

using namespace std;

Square::Square() : Shape()
{
	lenght = 0.0;
}

bool Square::SetData()
{
	cout << "============= SQUARE =============\n\n";
	cout << "Enter lenght:\n> ";

	cin >> lenght;

	return ValidateData(lenght);
}

void Square::Calculate()
{
	perimeter = 4 * lenght;
	area = lenght * lenght;
}

void Square::PrintResult()
{
	cout << "============= SQUARE =============\n\n";
	cout << "> lenght = " << lenght;
	cout << "\n\n> perimeter = " << perimeter;
	cout << "\n> area = " << area;
	cout << "\n\n";
	cout << "**********************************\n";
	cout << "*     Press Enter to continue    *\n";
	cout << "**********************************\n";

}

char * Square::GetName()
{
	return "Square";
}
