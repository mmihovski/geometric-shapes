#pragma once 

#include "Shape.h"

class Circle : public Shape
{
	private:
		double radius;

	public:
		Circle::Circle();

		bool SetData();
		void Calculate();
		void PrintResult();
		char* GetName();
};