#pragma once 

#include "Shape.h"

class Square : public Shape
{
	private:
		double lenght;

	public:
		Square::Square();

		bool SetData();
		void Calculate();
		void PrintResult();
		char* GetName();
};