# Geometric Shapes

This is a console program writen in C++ to calculate the perimeter and area of various 2D geometric shapes:
* square
* rectangle
* triangle
* circle

The program has a menu where the user selects the type of shape. For each shape the user enters fundamental data about its size. The program calculates the area and perimeter by first validating the entered values. The calculated values can be exported to a text file.

The program uses fundamental concepts of object-oriented programming and the C++ language - classes and objects, encapsulation, inheritance, function overloading, function overriding, virtual member functions, operator overloading.

